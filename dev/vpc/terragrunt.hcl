# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "git@bitbucket.org:danygarnier/test-module.git//vpc"
//  source = "../../../test-module/vpc/"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {
  cidr = "10.15.52.0/24"
  azs = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  private_subnets     = ["10.15.52.0/26", "10.15.52.64/26", "10.15.52.128/26"]
  public_subnets      = ["10.15.52.192/28", "10.15.52.208/28", "10.15.52.224/28"]
  enable_dns_support = true
  enable_dns_hostnames = true
  enable_nat_gateway = true
}
