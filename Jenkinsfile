@Library('sharedlib-ito@master') _

import com.els.itops.services.DevOpsConf
import com.els.itops.services.Utilities
import com.els.itops.utilities.Terragrunt

node('h2o-aws-slave') {
  if (currentBuild.number == 1) {
    currentBuild.result = 'ABORTED'
    error('ABORT because it\'s the first build.')
  }

  properties([
      parameters([
        string(defaultValue: 'master', description: 'For the repo live', name: 'BRANCH_NAME'),
        string(defaultValue: '857497588024', description: 'For Assume Role', name: 'ROLE_ACCOUNT'),
        string(defaultValue: 'itops', description: 'For Assume Role', name: 'ROLE_APP_NAME'),
        string(defaultValue: 'sb', description: 'For Assume Role', name: 'ROLE_ENV'),
        string(defaultValue: 'Jenkins', description: 'For Assume Role', name: 'ROLE_NAME'),
        string(defaultValue: 'eu-west-1', description: 'For Terraform variable', name: 'REGION'),
        string(defaultValue: 'testdany', description: 'For Terraform variable', name: 'APP_NAME'),
        string(defaultValue: 'dev', description: 'For Terraform variable', name: 'ENV'),
        choice(choices: ['vpc'], description: 'MODULE', name: 'MODULE'),
        string(defaultValue: 'git@bitbucket.org:danygarnier/test-live.git', description: 'git_url_live', name: 'git_url_live'),
        string(defaultValue: 'jenkins', description: 'aws_credentials_id', name: 'aws_credentials_id'),
        string(defaultValue: 'PLAN-APPLY', description: 'terraform_command', name: 'terraform_command'),
        string(defaultValue: 'terraform-12-20', description: 'terraform_version', name: 'terraform_version'),
        string(defaultValue: 'v0.19.4', description: 'terragrunt version', name: 'terragrunt_version')
      ]),
      buildDiscarder(
        logRotator(
          daysToKeepStr: '10',
          numToKeepStr: '3')
      ),
      disableConcurrentBuilds()
    ]),

def call(Map parameters = [:]) {

    //Global Credentials
    def credentials = new DevOpsConf()

    //CONSTANT DevOpsConf
    def GIT_CREDENTIALS_ID = credentials.GIT_CREDENTIALS_ID
    def AWS_CREDENTIALS_ID = credentials.AWS_CREDENTIALS_ID.get(parameters.get("aws_credentials_id"))
    def TERRAFORM_ACTION = [plan: 'PLAN-APPLY', plan_destroy: 'PLAN-DESTROY', apply: 'APPLY', destroy: 'DESTROY']

    //Form Field Variable
    branch_name = params.BRANCH_NAME
    def role = computeRole(params.ROLE_ACCOUNT, params.ROLE_APP_NAME, params.ROLE_ENV, params.ROLE_NAME)
    def additional_parameters = computeAdditionalParameters(params)
    def working_dir = computeWorkingDir(params.ENV, params.MODULE)
    def environment_variable = computeEnvironmentVariable(params.REGION, params.APP_NAME, params.ENV)

    //Function parameter
    def git_url_live = parameters.get("git_url_live")
    def terraform_command = parameters.get("terraform_command")
    def terraform_version = parameters.get("terraform_version")
    def terragrunt_version = parameters.get("terragrunt_version")

    //Constants and Utilities
    def utilities = new Utilities()
    def terragrunt = new Terragrunt()

    def build_ok = true

])

    stage('CleanUp Workspace') {
        ansiColor('xterm') {
            deleteDir()
            disableConcurrentBuilds()

            echo "\n GIT_CREDENTIALS \n ${GIT_CREDENTIALS_ID}"
            echo "\n AWS CREDENTIALS \n ${AWS_CREDENTIALS_ID}"
            echo "\n VARIABLE ENV \n ${environment_variable}"
            echo "\n ROLE \n ${role}"
            echo "\n WORKING DIR \n ${working_dir}"
            echo "\n ADDITIONAL PARAMETERS \n ${additional_parameters}"

        }
    }

    stage('Checkout Project') {
        ansiColor('xterm') {
            dir("live") {
                utilities.gitCheckout(git_url_live, branch_name, GIT_CREDENTIALS_ID)
            }
        }
    }

    stage('Installation Terraform') {
        ansiColor('xterm') {
            terragrunt.downloadAndInstallTerraform(terraform_version)
        }
    }

    stage('Installation Terragrunt') {
        ansiColor('xterm') {
            terragrunt.downloadAndInstall(terragrunt_version)
        }
    }

    // For PLAN FOR APPLY
    if (terraform_command == TERRAFORM_ACTION.plan || terraform_command == TERRAFORM_ACTION.apply) {
        try{
            stage('Plan Terraform') {
                ansiColor('xterm') {
                    //catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                        terragrunt.planApply(AWS_CREDENTIALS_ID, terraform_version, environment_variable, role, working_dir, additional_parameters)
                    //}
                }
            }
        } catch(e) {
        build_ok = false
        echo e.toString()
        }
    }

    // For APPLY
    if (terraform_command == TERRAFORM_ACTION.apply) {
        ansiColor('xterm') {
            stage('Approve Apply') {
                input 'Do you approve to apply the resources?'
            }
            stage('Apply Terraform') {
                ansiColor('xterm') {
                    catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                        terragrunt.apply(AWS_CREDENTIALS_ID, terraform_version, environment_variable, role, working_dir, additional_parameters)
                    }
                }
            }
        }
    }

    // For PLAN FOR DESTROY
    if (terraform_command == TERRAFORM_ACTION.plan_destroy || terraform_command == TERRAFORM_ACTION.destroy) {
        stage('Plan Terraform') {
            ansiColor('xterm') {
                terragrunt.planDestroy(AWS_CREDENTIALS_ID, terraform_version, environment_variable, role, working_dir, additional_parameters)
            }
        }
    }

    // For DESTROY
    if (terraform_command == TERRAFORM_ACTION.destroy) {
        ansiColor('xterm') {
            stage('Approve Destroy') {
                input 'Do you approve to destroy the resources?'
            }
            stage('Destroy Terraform') {
                ansiColor('xterm') {
                    terragrunt.destroy(AWS_CREDENTIALS_ID, terraform_version, environment_variable, role, working_dir, additional_parameters)
                }
            }
        }
    }
    // Stage added by Dany Garnier, 14/04/2020 : To clean up workspace at the end of the job
    stage('CleanUp Workspace after deploy') {
        ansiColor('xterm') {
            deleteDir()
        }
    }

     if(build_ok) {
        currentBuild.result = "SUCCESS"
    } else {
        currentBuild.result = "FAILURE"
    }
}
//fin node
}

def computeEnvironmentVariable(String region, String app_name, String env) {
    def environment_variable = ""

    if (region) {
        environment_variable += "export TF_VAR_app_name='${app_name}' "
    }

    if (app_name) {
        environment_variable += "export TF_VAR_region='${region}' "
    }
    if (env) {
        environment_variable += "export TF_VAR_env='${env}' "
    }

    return environment_variable
}

def computeRole(String role_account, String role_app_name, String role_env, String role_name) {
    def role = "arn:aws:iam::${role_account}:role/${role_app_name}"

    if (role_env) {
        role += "-${role_env}"
    }
    if (role_name) {
        role += "-${role_name}"
    }

    return role

}

def computeWorkingDir(String env, String module) {
    def working_dir = "${WORKSPACE}/live"

    if (env) {
        working_dir += "/${env}"
    }

    if (module && module != "ALL") {
        working_dir += "/${module}"
    }

    return working_dir
}

def computeAdditionalParameters(Map params) {
    def additional_parameters = ""
    for (entry in params) {
        if (entry.key.startsWith('TG_VAR_')) {
            key = entry.key.replace('TG_VAR_', '')
            key = key.toLowerCase()
            additional_parameters += " -var '${key}=${entry.value}' "
        }
    }

    return additional_parameters
}