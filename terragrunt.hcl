remote_state {
  backend = "s3"
  config = {

    bucket         ="els-${get_aws_account_id()}-${get_env("TF_VAR_app_name","dummy")}-${get_env("TF_VAR_env","dummy")}-${get_env("TF_VAR_region","dummy")}-tfstate"
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = "${get_env("TF_VAR_region","dummy")}"
    encrypt        = true
    dynamodb_table = "terraform-test-dany-lock-table"

    s3_bucket_tags = {
      project= "${get_env("TF_VAR_app_name","dummy")}"
      env="${get_env("TF_VAR_env","dummy")}"
      owner = "IT-O"
      dcp="yes"
      ics="yes"
    }

    dynamodb_table_tags = {
      project= "${get_env("TF_VAR_app_name","dummy")}"
      env = "${get_env("TF_VAR_env","dummy")}"
      owner = "IT-O"
      name  = "Terraform lock table"
    }
  }
}

inputs = {
  allowed_account_ids = ["857497588024"]
}
